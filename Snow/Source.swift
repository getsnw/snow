//
//  Source.swift
//  Snow
//
//  Created by Luke Chambers on 12/2/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftyJSON
import FileKit

struct Source {
    let url: URL
    let added: Bool
    let name: String
    let description: String
    let flakes: [String: URL]
}

func getSourceList() -> [URL]? {
    do {
        let sourcesString = try TextFile(path: .snowSources).read()
        let sourcesJson = JSON(parseJSON: sourcesString)
        var sources = [URL]()
        sourcesJson.arrayValue.forEach { json in
            if let urlString = json.string {
                if let url = URL(string: urlString) {
                    sources.append(url)
                }
            }
        }
        return sources
    } catch {
        return nil
    }
}

func loadSource(_ url: URL) -> (error: SourceError?, source: Source?) {
    // Load Source.json
    guard let json = url.appendingPathComponent("Source.json").loadJson() else {
        return (error: .failedToLoad, source: nil)
    }
    
    // Get info from Source.json
    guard let sourceName = json["name"].string else {
        return (error: .noNameKey, source: nil)
    }
    let sourceDescription = json["description"].string ?? url.absoluteString
    
    // Load Flakes/Flakes.json
    let flakesUrl = url.appendingPathComponent("Flakes", isDirectory: true)
    let flakesJsonUrl = flakesUrl.appendingPathComponent("Flakes.json")
    guard let flakesJson = flakesJsonUrl.loadJson() else {
        return (error: .failedToLoadFlakes, source: nil)
    }
    
    // Get flakes from Flakes/Flakes.json
    var flakes = [String: URL]()
    flakesJson.forEach { identifier, directory in
        if let unwrappedDir = directory.string {
            let flakeUrl = flakesUrl.appendingPathComponent(unwrappedDir)
            flakes[identifier] = flakeUrl
        }
    }
    
    // Get added sources and check if this source is installed
    guard let sources = getSourceList() else {
        return (error: .failedToLoadSourceList, source: nil)
    }
    let added = sources.contains(url)
    
    // Create and return a Source
    let source = Source(url: url, added: added, name: sourceName, description: sourceDescription, flakes: flakes)
    return (error: nil, source: source)
}

enum SourceError: String {
    case failedToLoad = "Failed to load the source's Source.json file."
    case noNameKey = "The source's Source.json file doesn't contain a name."
    case failedToLoadFlakes = "Failed to load the source's Flakes/Flakes.json file."
    case failedToLoadSourceList = "Failed to load the source list."
}
