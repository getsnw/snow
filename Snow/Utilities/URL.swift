//
//  URL.swift
//  Snow
//
//  Created by Luke Chambers on 12/6/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import Alamofire

extension URL {
    var isUp: Bool {
        let semaphore = DispatchSemaphore(value: 0)
        var up = false
        AF.request(self, method: .head).validate().response(queue: DispatchQueue.global(qos: .default)) { response in
            up = response.error == nil
            semaphore.signal()
        }
        semaphore.wait()
        return up
    }
}
