//
//  File.swift
//  Snow
//
//  Created by Luke Chambers on 12/3/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import FileKit

extension Path {
    static let snow = Path("/var/root/Snow")
    static let snowSources = snow + "Sources.json"
}
