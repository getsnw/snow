//
//  JSON.swift
//  Snow
//
//  Created by Luke Chambers on 12/2/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftCLI

extension URL {
    func loadJson() -> JSON? {
        do {
            let jsonData = try Data(contentsOf: self)
            return try JSON(data: jsonData)
        } catch {
            return nil
        }
    }
}

extension Command {
    func outputJson(_ output: [String: Any] = [:]) {
        var finalOutput = output
        if !finalOutput.keys.contains("error") {
            finalOutput["error"] = false
        }
        
        if let json = JSON(finalOutput).uglyString {
            print(json)
        }
    }
    
    func error(message: String, json: Bool) throws {
        if !json {
            throw message
        } else {
            let json: [String: Any] = [
                "error": true,
                "errorMessage": message
            ]
            outputJson(json)
        }
    }
}

extension JSON {
    var uglyString: String? {
        self.rawString(options: [])
    }
}
