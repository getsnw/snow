//
//  String.swift
//  Snow
//
//  Created by Luke Chambers on 12/2/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation

extension String: LocalizedError {
    public var errorDescription: String? { return self }
}

extension String {
    var indented: String { "    \(self)" }
    var snowEmoji: String { "❄️  \(self)" }
    
    func replacingFirstOccurrence(of string: String, with replacement: String) -> String {
        guard let range = self.range(of: string) else { return self }
        return replacingCharacters(in: range, with: replacement)
    }
    
    func deletingSuffix(_ suffix: String) -> String {
        guard self.hasSuffix(suffix) else { return self }
        return String(self.dropLast(suffix.count))
    }
    
    var normalizedUrl: URL? {
        var normalized = self
            .lowercased()
            .replacingOccurrences(of: "http://", with: "")
            .replacingOccurrences(of: "https://", with: "")
        while normalized.contains("//") {
            normalized = normalized.replacingOccurrences(of: "//", with: "/")
        }
        if normalized.starts(with: "www.") {
            normalized = normalized.replacingFirstOccurrence(of: "www.", with: "")
        }
        if normalized.last == "/" {
            normalized.removeLast()
        }
        normalized = "https://" + normalized
        return URL(string: normalized)
    }
    
    var normalizedVersionString: String {
        var normalized = self
        while normalized.hasSuffix(".0") {
            normalized = normalized.deletingSuffix(".0")
        }
        if !normalized.contains(".") {
            normalized = "\(normalized).0"
        }
        return normalized
    }
    
    func s(_ int: Int) -> String {
        return int == 1 ? self : "\(self)s"
    }
}

extension Optional {
    var optionalString: String? {
        if self == nil {
            return nil
        } else {
            return "\(self!)"
        }
    }
}
