//
//  main.swift
//  Snow
//
//  Created by Luke Chambers on 12/2/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftCLI
import FileKit
import SwiftyJSON

// Check for and create the Snow directory and its files
do {
    if !Path.snow.exists {
        try Path.snow.createDirectory()
    }
    if !Path.snowSources.exists {
        let emptyList = JSON([
            "https://getsnw.gitlab.io/storm"
        ]).uglyString ?? "[]"
        try emptyList |> TextFile(path: .snowSources)
    }
} catch {
    print("Error: Failed to access /var/root/Snow. Make sure you're running Snow as root.")
    exit(1)
}

// Create the CLI and add its commands
let snowCli = CLI(name: "snow", version: "1.0", description: "Lightweight package manager for jailbroken iOS devices")
snowCli.commands = [
    InfoCommand(),
    SourceGroup()
]

// Add a global API flag
let apiFlag = Flag("-a", "--api", description: "Prints the output of this command in JSON.")
extension Command {
    var api: Flag {
        return apiFlag
    }
}
snowCli.globalOptions.append(apiFlag)

// Start the CLI
let _ = snowCli.go()
