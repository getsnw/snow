//
//  InfoCommand.swift
//  Snow
//
//  Created by Luke Chambers on 12/5/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftCLI

class InfoCommand: Command {
    let name = "info"
    let shortDescription = "Get info about a snowflake"
    
    let identifier = Parameter()
    
    func execute() throws {
        // Load the flake and handle errors if needed
        let loadedFlake = getFlakeInfo(of: identifier.value.lowercased())
        if let loadError = loadedFlake.error {
            try error(message: loadError.rawValue, json: api.value)
            return
        }
        
        // Get the actual flake
        guard let flake = loadedFlake.flake else {
            try error(message: "An unexpected error occurred while attempting to load the snowflake.", json: api.value)
            return
        }
        
        // Print the output
        if !api.value {
            print()
            print("Info for \("\(flake.identifier)".lightBlue.bold):")
            print()
            print("\("Source:".lightBlue) \(flake.source.url)".indented)
            print("\("Name:".lightBlue) \(flake.name)".indented)
            print("\("Description:".lightBlue) \(flake.description)".indented)
            print("\("Version:".lightBlue) \(flake.version)".indented)
            print("\("Authors:".lightBlue) \(flake.authors.joined(separator: ", "))".indented)
            print("\("iOS Versions:".lightBlue) \(flake.minSupport ?? "")-\(flake.maxSupport ?? "")".indented)
            print()
            print("This snowflake has \("\(flake.downloads.count)".lightBlue.bold) \("download".s(flake.downloads.count)):")
            print()
            flake.downloads.forEach { type, url in
                print("\(type.rawValue.lightBlue): \(url)".indented)
            }
            print()
        } else {
            outputJson([
                "identifier": flake.identifier,
                "source": "\(flake.source.url)",
                "name": flake.name,
                "description": flake.description,
                "version": flake.version,
                "authors": flake.authors,
                "support": [
                    "minimum": flake.minSupport,
                    "maximum": flake.maxSupport
                ]
            ])
        }
    }
}
