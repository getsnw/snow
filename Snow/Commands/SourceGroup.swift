//
//  SourceGroup.swift
//  Snow
//
//  Created by Luke Chambers on 12/2/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftCLI
import Rainbow
import SwiftyJSON
import FileKit

class SourceGroup: CommandGroup {
    let name = "source"
    let shortDescription = "Manage your added sources"
    
    let children: [Routable] = [
        SourceAddCommand(),
        SourceInfoCommand(),
        SourceListCommand(),
        SourceRemoveCommand()
    ]
}

class SourceInfoCommand: Command {
    let name = "info"
    let shortDescription = "Get info about a source"
    
    let url = Parameter()
    
    func execute() throws {
        // Get the actual URL
        guard let actualUrl = url.value.normalizedUrl else {
            try error(message: "The URL you specified is invalid.", json: api.value)
            return
        }
        
        // Load the source and handle errors if needed
        let loadedSource = loadSource(actualUrl)
        if let loadError = loadedSource.error {
            try error(message: loadError.rawValue, json: api.value)
            return
        }
        
        // Get the actual source
        guard let source = loadedSource.source else {
            try error(message: "An unexpected error occurred while attempting to load the source.", json: api.value)
            return
        }
        
        // Print the output
        if !api.value {
            print()
            print("Info for \("\(source.url)".lightBlue.bold):")
            print()
            print("\("Added:".lightBlue) \(source.added ? "yes" : "no")".indented)
            print("\("Name:".lightBlue) \(source.name)".indented)
            print("\("Description:".lightBlue) \(source.description)".indented)
            print()
            print("This source has \("\(source.flakes.count)".lightBlue.bold) \("snowflake".s(source.flakes.count)):")
            print()
            source.flakes.forEach { identifier, url in
                let directory = url.lastPathComponent
                print("\("\(identifier):".lightBlue) /Flakes/\(directory)".indented)
            }
            print()
        } else {
            outputJson([
                "url": "\(source.url)",
                "added": source.added,
                "name": source.name,
                "description": source.description,
                "flakes": source.flakes.mapValues { "\($0)" }
            ])
        }
    }
}

class SourceListCommand: Command {
    let name = "list"
    let shortDescription = "Get a list of added sources"
    
    func execute() throws {
        // Get the source list
        guard let sources = getSourceList() else {
            try error(message: "Failed to get sources.", json: api.value)
            return
        }
        
        // Print the output
        if !api.value {
            print()
            print("You have \("\(sources.count)".lightBlue.bold) \("source".s(sources.count)):")
            print()
            sources.forEach { url in
                print("\(url)".lightBlue.indented)
            }
            print()
        } else {
            outputJson(["sources": sources.map { "\($0)" }])
        }
    }
}

class SourceAddCommand: Command {
    let name = "add"
    let shortDescription = "Add a source"
    
    let url = Parameter()
    
    func execute() throws {
        // Get the actual URL
        guard let actualUrl = url.value.normalizedUrl else {
            try error(message: "The URL you specified is invalid.", json: api.value)
            return
        }
        
        // Get sources and check if it's already added
        guard var sources = getSourceList() else {
            try error(message: "Failed to get sources.", json: api.value)
            return
        }
        if sources.contains(actualUrl) {
            try error(message: "That source has already been added.", json: api.value)
            return
        }
        
        // Validate the source
        let loadError = loadSource(actualUrl).error
        if let unwrappedError = loadError {
            try error(message: unwrappedError.rawValue, json: api.value)
            return
        }
        
        // Add the source
        sources.append(actualUrl)
        let stringSources = sources.map { "\($0)" }
        guard let newJson = JSON(stringSources).uglyString else {
            try error(message: "Failed to add source.", json: api.value)
            return
        }
        do {
            try newJson |> TextFile(path: .snowSources)
        } catch {
            try self.error(message: "Failed to save sources file.", json: api.value)
        }
        
        // Print a success message
        if !api.value {
            print("Successfully added \("\(actualUrl)".lightBlue.bold).".snowEmoji)
        } else {
            outputJson()
        }
    }
}

class SourceRemoveCommand: Command {
    let name = "remove"
    let shortDescription = "Remove a source"
    
    let url = Parameter()
    
    func execute() throws {
        // Get the actual URL
        guard let actualUrl = url.value.normalizedUrl else {
            try error(message: "The URL you specified is invalid.", json: api.value)
            return
        }
        
        // Get the list of sources
        guard var sources = getSourceList() else {
            try error(message: "Failed to get sources.", json: api.value)
            return
        }
        
        // Remove the source
        guard let index = sources.firstIndex(of: actualUrl) else {
            try error(message: "That source isn't added.", json: api.value)
            return
        }
        sources.remove(at: index)
        let stringSources = sources.map { "\($0)" }
        guard let newJson = JSON(stringSources).uglyString else {
            try error(message: "Failed to remove source.", json: api.value)
            return
        }
        do {
            try newJson |> TextFile(path: .snowSources)
        } catch {
            try self.error(message: "Failed to save sources file.", json: api.value)
        }
        
        // Print a success message
        if !api.value {
            print("Successfully removed \("\(actualUrl)".lightBlue.bold).".snowEmoji)
        } else {
            outputJson()
        }
    }
}
