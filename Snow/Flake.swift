//
//  Flake.swift
//  Snow
//
//  Created by Luke Chambers on 12/2/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import Version

struct Flake {
    let identifier: String
    let source: Source
    let name: String
    let description: String
    let version: String
    let authors: [String]
    let minSupport: String?
    let maxSupport: String?
    let downloads: [FlakeDownloadType: URL]
}

func getFlakeInfo(of identifier: String) -> (error: FlakeError?, flake: Flake?) {
    // Get all URLs for this identifier
    guard let sources = getSourceList() else {
        return (error: .failedToLoadSourceList, flake: nil)
    }
    var candidates = [(url: URL, source: Source)]()
    sources.forEach { source in
        if let actualSource = loadSource(source).source {
            let flakeUrl = actualSource.flakes[identifier]?
                .appendingPathComponent("Flake.json")
            if let url = flakeUrl {
                candidates.append((url: url, source: actualSource))
            }
        }
    }
    
    // Parse each of the flake files
    var latest: Flake? = nil
    for candidate in candidates {
        if let json = candidate.url.loadJson() {
            // Gather flake info
            guard let flakeName = json["name"].string else {
                continue
            }
            let flakeDescription = json["description"].string ?? identifier
            guard let flakeVersion = json["version"].string?.normalizedVersionString else {
                continue
            }
            let flakeAuthors = json["authors"].arrayValue
                .filter { $0.string != nil }
                .map { $0.stringValue }
            let flakeMinSupport = json["support"]["minimum"].string?.normalizedVersionString
            let flakeMaxSupport = json["support"]["maximum"].string?.normalizedVersionString
            
            // Prepare to check for flake downloads
            var flakeDownloads = [FlakeDownloadType: URL]()
            let flakeDirectory = candidate.url.deletingLastPathComponent()
            
            // Check for .app
            let flakeAppUrl = flakeDirectory.appendingPathComponent("Flake.app.zip")
            if flakeAppUrl.isUp {
                flakeDownloads[.app] = flakeAppUrl
            }
            
            // Check for .ipa
            let flakeIpaUrl = flakeDirectory.appendingPathComponent("Flake.ipa")
            if flakeIpaUrl.isUp {
                flakeDownloads[.ipa] = flakeIpaUrl
            }
            
            // Check for .deb
            let flakeDebUrl = flakeDirectory.appendingPathComponent("Flake.deb")
            if flakeDebUrl.isUp {
                flakeDownloads[.deb] = flakeDebUrl
            }
            
            // Create the flake
            let flake = Flake(
                identifier: identifier,
                source: candidate.source,
                name: flakeName,
                description: flakeDescription,
                version: flakeVersion,
                authors: flakeAuthors,
                minSupport: flakeMinSupport,
                maxSupport: flakeMaxSupport,
                downloads: flakeDownloads
            )
            
            // Replace latest with this flake if version is newer
            if let newVersion = Version(flake.version), let oldVersion = Version(latest?.version ?? "0") {
                if newVersion > oldVersion {
                    latest = flake
                }
            }
        }
    }
    
    // Return the flake or an error if there's no flake
    if let flake = latest {
        return (error: nil, flake: flake)
    } else {
        return (error: .noSnowflakes, flake: nil)
    }
}

enum FlakeDownloadType: String {
    case app = "App"
    case ipa = "IPA"
    case deb = "DEB"
}

enum FlakeError: String {
    case failedToLoadSourceList = "Failed to load the source list."
    case noSnowflakes = "No snowflakes have that identifier."
}
